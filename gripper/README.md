# Envoi du GRIP !

### Introduction
Ce sous-projet consiste au **retroengineering** d'une pince OnRobot RG-2. Nous nous intéresserons ici purement à la partie électronique et informatique de la pince.

### Matériel
Le matériel nécessaire est le suivant :
- Un Arduino Nano.
- Deux steppers 28BYJ-48 5V.
- Deux cartes de contrôle ULN2003.
- Deux abaisseurs de tension 24V-12V et 24V-5V.
- Un connecteur RKMV 8-354.

### Montage
Un schéma du montage électrique à effectué est disponible dans le fichier **./schema_elec.pdf**.

### Utilisation
