#include <Stepper.h>
#include <EEPROM.h>

#define STEPPER_PIN_1 9
#define STEPPER_PIN_2 10
#define STEPPER_PIN_3 11
#define STEPPER_PIN_4 12
#define OPEN_PIN 2
#define CLOSE_PIN 3
#define WIDTH_PIN 5

const int STEPS_PER_REVOLUTION = 4096;
const int MAX_STEPS = STEPS_PER_REVOLUTION/8;

int currentStep = 0;

Stepper stepper = Stepper(STEPS_PER_REVOLUTION, STEPPER_PIN_1, STEPPER_PIN_3, STEPPER_PIN_2, STEPPER_PIN_4);

void step(int n) {
    currentStep += n;
    EEPROM.put(0, currentStep);
    digitalWrite(LED_BUILTIN, HIGH);
    stepper.step(n);
    digitalWrite(LED_BUILTIN, LOW);
}

int grip_width() {
    float ratio = ((float) currentStep)/((float) MAX_STEPS) + 1.0; // Ratio is between 0 and 2
    int analogSig = floor(255.0*(ratio/2.0)); // It is then converted to an int between 0 and 255
    return analogSig;
}

void setup() {
    currentStep = EEPROM.read(0);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(OPEN_PIN, INPUT);
    pinMode(CLOSE_PIN, INPUT);
    stepper.setSpeed(4);
    Serial.begin(9600);
}

void loop() {
    Serial.print(digitalRead(OPEN_PIN));
    Serial.print(" ");
    Serial.print(digitalRead(CLOSE_PIN));
    Serial.print(" ");
    
    if (digitalRead(CLOSE_PIN) == LOW && digitalRead(OPEN_PIN) == HIGH && currentStep > -MAX_STEPS) {
        step(-8);
    } else if (digitalRead(OPEN_PIN) == LOW && digitalRead(CLOSE_PIN) == HIGH && currentStep < MAX_STEPS) {
        step(8);
    }

    Serial.println(grip_width());
    analogWrite(WIDTH_PIN, grip_width());
}
