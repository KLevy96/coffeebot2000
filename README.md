# COFFEEBOT2000

## Introduction

Ce projet a pour but de réaliser un système autonome de pick-and-place, composé d'un chariot MiR200, d'un bras robotisé UR10, et d'une pince réalisée en interne.  
En pratique, le système a pour but de servir du café directement au bureau des employés de MMDB - Envoi du Net. Il vient au bureau de l'employé et détecte la position d'un AprilTag, déplace la pince pour attraper une tasse, l'amène à une machine à café (connectée au réseau) pour préparer une boisson, et enfin ramène la tasse en redétectant la position de l'AprilTag.  
Plusieurs sous-projets existent donc : la pince compatible avec un UR10, la machine à café connectée, la gestion du bras et de la détection des AprilTags, et enfin, la gestion du MiR.  

## Installation

#### Configuration Physique
Le système est entièrement géré par un PC qui doit être embarqué sur le Coffeebot. Une caméra rectifiée (Intel Realsense D435) et un adaptateur Ethernet connecté à l'UR10 sont à connecter sur le PC.  
Par défaut, la communication avec l'UR10 est assurée via une connection Ethernet physique. Le robot doit avoir l'adresse IP 192.168.1.3, et le PC doit avoir l'adresse 192.168.1.1. Ces adresses peuvent-être modifiées dans le docker-compose, mais elles devront également être modifiées sur le robot.  
Evidemment, le bras doit être démarré et prêt avant de pouvoir utiliser le système. Aucun programme n'est à lancer en revanche, car un service s'occupe de tout lancer lorsque nécessaire.  
Enfin, les joints du bras doivent être placés dans la configuration suivante :
- Base : 270°
- Shoulder : -45°
- Elbow : -135°
- Wrist1 : -180°
- Wrist2 : -90°
- Wrist3 : 180°
Ceci assure que le bras parte d'une position où il sera toujours possible d'atteindre sa cible. Avant de picker une tasse, la pince doit être ouverte.

#### Docker
Un docker-compose définit tous les services nécessaires :
- roscore : un service qui agit comme coeur de communication pour tous les noeuds ROS
- rosurdriver : le noeud qui permet d'intéragir avec l'UR10
- rosmoveit : un noeud de calcul pour trouver des chemins complexes pour le bras
- roscamera : un noeud publiant les données en provenance de la camera embarquée
- rosapriltags : un service qui détecte les AprilTags dans un flux vidéo, et donne leur position
- rosaprilpicker : un service développé en interne qui expose les routes nécessaires pour picker une tasse sur un tag, placer une tasse, et positionner la tasse pour la machine à café
- rosrviz : une interface de visualisation du système
- rosclient : un client de débug, majoritairement inutile.
- mirdriver : un service qui gère à la fois le MiR et le bras pour envoyer des tâches. C'est l'interface principale entre l'utilisateur et le système.

Le lancement des ces services doit se faire de façon séquentielle :
1. roscore
2. rosurdriver & roscamera
3. rosmoveit & rosapriltags
4. rosaprilpicker
5. mirdriver

Il est important de veiller à s'assurer que tous les services tournent bien avant de lancer des tâches.

#### Détection de Tag
Le système est fait pour détecter un AprilTag n°1 36h11 sur le bureau de l'utilisateur. Le tag peut être placé dynamiquement, aussi la solution retenue est de le coller sur un dessou de verre dans cette utilisation.  
Sur la machine à café, un AprilTag n°2 36h11 doit être placé afin de noter la position relative de la machine dans l'espace. Dans le script *./ur_coffeebot/rosurcommon/src/aprilpicker/src/scripts/aprilpicker.py* sont notées deux variables **tcp_offset_** qui représentent le décalage entre le TCP de l'UR10 et la position cible de la pince du robot.  
La configuration des tags détectables et de leurs tailles est disponible dans le fichier *./ur_coffeebot/rosurcommon/src/apriltag_ros/apriltag_ros/config/tags.yaml*.

#### Machine à Café
Lorsque le robot arrive à sa machine à café de destination, il doit pouvoir détecter le tag présenté dans la partie **Détection de Tag**. Le bras va placer la tasse à l'endroit précisé dans *./ur_coffeebot/rosurcommon/src/aprilpicker/src/scripts/aprilpicker.py* par rapport au tag détecté, et dans la fonction *make_coffee()* de ce même script, le robot appellera une route HTTP GET pour faire un café.  
La machine doit donc être capable de lancer un café lorsque cette route est appelée. Dans la démonstration, un ESP8266 connecté au réseau WiFi ponte le bouton qui permet de faire un café lorsqu'on appelle la route */coffee* sur une machine à café qui a été modifiée pour se faire. Le code de l'ESP est contenu dans le dossier *./esp/*.  
N'importe quelle machine à café est donc utilisable avec quelques compétences basiques en Internet des Objets et en électronique.

N.B. : Dans le démonstration, on doit s'assurer que la machine est prête à faire un café, i.e. qu'il y ait assez d'eau, que le reservoir de marc n'est pas plein, que la machine soit allumée, qu'il reste du café, ...

#### MiR
Enfin, le MiR doit être paramétré pour deisposer d'au moins cinq missions :
- **Charge** : Mission de charge dans laquelle le robot se dock à sa station de charge, qui doit être interrompue dès qu'une nouvelle mission est ajoutée au robot.
- **PreCafe** : Position intermédiaire d'approche pour la machine à café.
- **Cafe** : Position d'où le robot ira placer la tasse dans la machine.
- **PostCafe** : Position intermédiaire de départ pour partir de la machine à café.
- **BureauXXX** : Position d'où le robot ira récupérer et placer la tasse de l'utilisateur. La syntaxe est par exemple **BureauKevin**.

Ces missions doivent remplir leur cahier des charges, toute fonctionnalité supplémentaire est optionnelle. Par exemple, dans la mission **BureauKevin**, le robot va à l'endroit indiqué d'où la dépose et la récupération est optimale, joue un son qui indique à l'utilisateur de mettre son tag à portée, et fait clignoter des petites LEDs. Seule l'assurance que le robot soit arrivé à la position indiquée est nécessaire.  
A chaque position de bureau, on doit s'assurer que le robot est bien capable de prendre et de poser de façon fiable, précise, et sécurisée la tasse de l'utilisateur. Les instructions nécessaires sont précisées dans la partie **Maintenance**.

## Utilisation

#### Basique
Le service *mir_driver* expose une route HTTP POST */serveCoffee*, prenant un objet JSON dans le corps de la requête qui doit contenir comme seul attribut **name** le nom de la mission correspondant à l'emplacement d'un utilisateur (i.e. **BureauMika**).  
N'importe quelle façon d'appeler cette route fonctionne donc, que ce soit via une super interface Web codée en React.js, une application Android dynamique à l'ergonomie maximale, ou avec un bon vieux *curl* des familles.  
Ex:
> $ curl -X POST -H 'Content-Type: application/json' -d '{"name": "BureauLudovic"}' http://adresse.ip.du.mirdriver:3000/serveCoffee

Le robot viendra alors au bureau précisé, cherchera un tag visible, viendra prendre une tasse qui a été placée sur le tag après détection de ce dernier, ira faire un café, puis enfin reviendra le poser sur le tag.

#### Avancée
Il est possible de tester tous ces micro-services manuellement.

**rosaprilpicker** expose 3 services ROS pour respectivement picker une tasse sur un tag, placer une tasse sur un tag, et enfin placer une tasse dans la machine à café. Ces services sont */coffeebot/pickMug*, */coffeebot/placeMug*, et */coffeebot/makeCoffee*. On peut appeler un service ROS d'une machine où ROS est installé et l'environnement est bien paramétré pour parler avec le bon core ROS via :
> $ rosservice call /coffeebot/pickMug (par exemple)

Avec l'interface du MiR (accessible via son adresse IP sur navigateur web), on peut aussi créer de nouvelles missions, de nouvelles positions, modifier les missions existantes, ... **mirdriver** propose aussi la route HTTP POST */playMission* qui prend de la même façon que */serveCoffee* un JSON, mais joue juste la mission envoyée.

**rosrviz** est un autre service qui permet d'avoir une visualisation en temps réel de la scène, très pratique pour le debug. La position relative de tous les objets est notée, et on peut même voir les tags détectés. Ces positions relatives sont éditables dans *./ur_coffeebot/rosurcommon/src/fmauch_universal_robot/model_moveit_config/launch/ur10_moveit_planning_execution_bare.launch*, et doivent être calibrées si la position de la caméra est modifiée.

## Maintenance

#### Ajout d'un Nouveau Bureau
Lorsqu'un nouveau bureau doit être géré, il faut noter la position d'approche sur l'interface du MiR, créer une mission associée en respectant la syntaxe **BureauNom**, et l'administrateur doit s'assurer qu'une tasse peut bien être pickée et placée, grâce aux routes */coffeebot/pickMug* et */coffeebot/placeMug* de **rosaprilpicker**.

#### Déplacement de la Caméra / Calibration
Changez les valeurs lignes 18 et 19 de *./ur_coffeebot/rosurcommon/src/fmauch_universal_robot/model_moveit_config/launch/ur10_moveit_planning_execution_bare.launch*.  
Les 6 premiers arguments suivent cette nomenclature : **x y z rz ry rx**
- *x* : position sur l'axe x en mètres (vers l'avant de la pince).
- *y* : position sur l'axe y en mètres (vers la gauche de la pince).
- *z* : position sur l'axe z en mètres (vers le dessus de la pince).
- *rz* : angle autour de l'axe z en radians.
- *ry* : angle autour de l'axe y en radians.
- *rx* : angle autour de l'axe x en radians.

Redémarrez le service **rosmoveit** pour prendre ces changements en compte.

N.B. : Faire un *docker-compose down* et redémarrer tous les services manuellement un par un est toujours une bonne idée.

#### Collision avec la Pince
- Appuyez sur le bouton d'Arrêt d'Urgence du bras (vite).  
- Redémarrez le service **rosaprilpicker** et **mirdriver**.  
- Videz la liste de mission via l'interface du MiR.  
- Repositionnez le bras aux angles indiqués dans la partie Installation - Configuration Physique, et rouvrez la pince.  
- Envoyez enfin le robot en charge; le robot est prêt pour de nouvelles collision trépidantes !