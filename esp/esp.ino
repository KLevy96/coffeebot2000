/**
  *  Authored by Kevin Levy
  *  On July 15th, 2021
  */

#include "ESP8266WiFi.h"
#include "ESPAsyncWebServer.h"

// Constants
#define COFFEE_PIN 5 // D1

// WiFi Credentials (change these if you want it to work !)
const char SSID[] = "EDN-BUREAUX";
const char PWD[] = "le delicieux wifi des bureaux";

// Web server (listening on port 80)
AsyncWebServer server(80);

bool isCoffeeQueued;

/**
 * Connects to WiFi using the constants SSID and PWD
 */
void connect_wifi() {
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);
    Serial.println("Attempting to connect to WiFi...");
    WiFi.begin(SSID, PWD);
    while (WiFi.status() != WL_CONNECTED) {
        delay(200);
        Serial.print('.');
    }
    Serial.println("WiFi Connected !");
}

/**
 * Prints IP and MAC address
 */
void print_network_status() {
    char s[256];
    sprintf(s, "\tIP address : %s\n", WiFi.localIP().toString().c_str());
    Serial.print(s);
    sprintf(s, "\tMAC address : %s\n", WiFi.macAddress().c_str());
    Serial.print(s);
}

/**
 * Sets up AsyncWebServer and routes
 */
void setup_http_server() {
    // Declaring root handler, and action to be taken when root is requested
    auto root_handler = server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        Serial.println("=> /");
        request->send(418);
    });

    // This route makes coffee lmao
    server.on("/coffee", HTTP_GET, [](AsyncWebServerRequest *request){
        Serial.println("=> /coffee");
        if (!isCoffeeQueued) {
            isCoffeeQueued = true;
            request->send(200);
        } else {
            request->send(400);
        }
    });

    // If request doesn't match any route, returns 404.
    server.onNotFound([](AsyncWebServerRequest *request){
        Serial.println("=> Unknown route");
        request->send(404);
    });

    server.begin();
}

void setup() {
    // Begins serial connection
    Serial.begin(9600);

    isCoffeeQueued = false;

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    pinMode(COFFEE_PIN, OUTPUT);
    digitalWrite(COFFEE_PIN, LOW);

    // Handles connection to the WiFi network
    connect_wifi();
    print_network_status();

    // Sets up HTTP server and routes
    setup_http_server();
}

void loop() {
    if (isCoffeeQueued) {
        digitalWrite(COFFEE_PIN, HIGH);
        delay(1000);
        digitalWrite(COFFEE_PIN, LOW);
        isCoffeeQueued = false;
    } else {
        delay(1000); 
    }
}
