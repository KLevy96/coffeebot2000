# ur_aprilpicker
## Introduction

This service serves 2 purposes:  
- The visualisation of the current state of the robot, its virtual 3D scene, and the image stream from the camera, enhanced with the detection of the april tags.  
- A web service that allows to get the current detected tags from the camera in a standard JSON format.  

## How to launch
Launching the service is as simple as launching the docker-compose at the root of this repo.  
When 
> $ docker-compose up

is used, several docker services are launched :  
- **roscore**, which is both a ros master for the communication of the ros nodes, and a websocket bridge.  
- **rosurdriver**, which is a driver for the UR10, allowing to expose several services to manipulate the robot.  
- **rosmoveit**, which is a standardized robot trajectory handler.  
- **roscamera**, which is a node allowing to stream the video feed from a camera to other nodes.  
- **rosapriltags**, which is a node taking the video stream from roscamera, and detects apriltags in the images.  
- **rosrviz**, which allows for the visualisation of the robot, and the camera's video stream.  
- **apriltag-detector**, which is a Node.js microservice that exposes a HTTP GET route in order to get the tags detected by the camera.  

**! Be careful !**  
Since RVIZ uses the Graphical User Interface, you need to run 
> $ xhost +local:docker
before launching the docker-compose.

## How to use
apriltag-detector exposes an HTTP GET route to get the detected tags on port 3000.  
The route is **/detectedTags**, and answers with a standard JSON array containing the ids of the detected tags as integers.  
Example :
> $ curl -X GET http://192.168.255.19:3000/detectedTags  
> [1,2,3,5,6]

Here, the service shows that the detected tags are 1, 2, 3, 5, and 6. Thus the tag 4 is not detected.

In order to resize the application windows for the 4k TV, you can use the **devilspie2** service, which will fit, stretch and undecorate the windows specified in **/home/edn/.config/devilspie2/test.lua**.  
You can script this feature using the *lua* language.

To run devilspie2, you need to run 
> $ devilspie2

That's it.  
Sometimes the windows aren't stretched quite right : you can just close devilspie2 and re-run it, this will do the trick.