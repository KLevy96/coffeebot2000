#!/usr/bin/env python
# Python 2/3 compatibility imports
from __future__ import print_function

import requests
import time
import copy
from rosgraph.masterapi import Error
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import rospy
import tf2_ros
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from std_srvs.srv import Trigger, TriggerResponse
from ur_dashboard_msgs.srv import Load, GetProgramState
from moveit_commander.conversions import pose_to_list
from apriltag_ros.msg import AprilTagDetectionArray, AprilTagDetection
import requests

in_use = False
tcp_offset_coffee = {'x': -0.43, 'y': -0.005, 'z': -0.235}
tcp_offset_tag = {'x': -0.3, 'y': 0.0, 'z': 0.06} # z 0.045

# robot kinematic model and current joint states
robot = moveit_commander.RobotCommander()

# robot understanding of surrounding world
scene = moveit_commander.PlanningSceneInterface()

# change group_name according to ur robot
group_name = "arm"
move_group = moveit_commander.MoveGroupCommander(group_name)

# display trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
     "/display_planned_path", moveit_msgs.msg.DisplayTrajectory, queue_size=20
)

# name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# print the entire state of the robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")

rospy.init_node('picker')

connect_dashboard = rospy.ServiceProxy('/ur_hardware_interface/dashboard/connect', Trigger)
loader_service  = rospy.ServiceProxy('/ur_hardware_interface/dashboard/load_program', Load)
player_service  = rospy.ServiceProxy('/ur_hardware_interface/dashboard/play', Trigger)
state_service   = rospy.ServiceProxy('/ur_hardware_interface/dashboard/program_state', GetProgramState)
urscript_pub = rospy.Publisher('/ur_hardware_interface/script_command', String, queue_size=1)

def cartesian_move(target, goBack):
	global move_group

	# Cartesian path planning to the destination 
	waypoints = []
	wpose = move_group.get_current_pose().pose

	if not goBack:
		if wpose.position.x > target.position.x :           
			while wpose.position.x > target.position.x : 
				wpose.position.x -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.x < target.position.x :
				wpose.position.x += 0.0005
				waypoints.append(copy.deepcopy(wpose))

		if wpose.position.y > target.position.y :
			while wpose.position.y > target.position.y :
				wpose.position.y -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.y < target.position.y :
				wpose.position.y += 0.0005
				waypoints.append(copy.deepcopy(wpose))

		if wpose.position.z > target.position.z :
			while wpose.position.z > target.position.z :
				wpose.position.z -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.z < target.position.z :
				wpose.position.z += 0.0005
				waypoints.append(copy.deepcopy(wpose))
	else:
		if wpose.position.z > target.position.z :
			while wpose.position.z > target.position.z :
				wpose.position.z -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.z < target.position.z :
				wpose.position.z += 0.0005
				waypoints.append(copy.deepcopy(wpose))

		if wpose.position.y > target.position.y :
			while wpose.position.y > target.position.y :
				wpose.position.y -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.y < target.position.y :
				wpose.position.y += 0.0005
				waypoints.append(copy.deepcopy(wpose))

		if wpose.position.x > target.position.x :           
			while wpose.position.x > target.position.x : 
				wpose.position.x -= 0.0005
				waypoints.append(copy.deepcopy(wpose))
		else : 
			while wpose.position.x < target.position.x :
				wpose.position.x += 0.0005
				waypoints.append(copy.deepcopy(wpose))

	# We want the Cartesian path to be interpolated at a resolution of 1 cm
	# which is why we will specify 0.01 as the eef_step in Cartesian
	# translation.  We will disable the jump threshold by setting it to 0.0,
	# ignoring the check for infeasible jumps in joint space, which is sufficient
	# for this tutorial.
	(plan, fraction) = move_group.compute_cartesian_path(
									waypoints,   # waypoints to follow
									0.0005,        # eef_step
									0.0)         # jump_threshold

	#executing the movement of robots following the waypoints
	time.sleep(1)
	move_group.execute(plan, wait=True)
	move_group.stop()
	move_group.clear_pose_targets()

	final_pose = move_group.get_current_pose().pose

	if final_pose.position.x < target.position.x - 0.01 or final_pose.position.x > target.position.x + 0.01 or final_pose.position.y < target.position.y - 0.01 or final_pose.position.y > target.position.y + 0.01 or final_pose.position.z < target.position.z - 0.01 or final_pose.position.z > target.position.z + 0.01:
		raise Error("Arm wasn't able to reach the tag's position.")

def try_cartesian_move(target, goBack):
	error_cpt = 0

	while error_cpt < 25:
		try:
			cartesian_move(target, goBack)
			break
		except Error as e:
			print(e)
			error_cpt += 1

			loader_service('external_control.urp')
			while state_service().program_name != 'external_control.urp':
				print(state_service())
				time.sleep(0.5)
			player_service()
			time.sleep(1)
	
	if error_cpt >= 25:
		raise Error("After 25 tries, the arm failed to reach the target position.")

def pick_mug(req):
	global move_group, tcp_offset_tag

	# the origin position of ur10, which can be editted by yourself if you wish to do so
	origin = move_group.get_current_pose().pose
	print("============ Printing origin")
	print(origin)
	print("")

	tfBuffer = tf2_ros.Buffer()
	listener = tf2_ros.TransformListener(tfBuffer)

	# getting the translation from base_link (base of robot) to tag_1 (any tag which is your destination)
	trans = tfBuffer.lookup_transform('base_link', 'tag_1', rospy.Time(), rospy.Duration(15))
	print("============ Printing tag_1 position")
	print("x: {}".format(trans.transform.translation.x))
	print("y: {}".format(trans.transform.translation.y))
	print("z: {}".format(trans.transform.translation.z))
	print("")

	requests.post('http://192.168.255.1:3000/playMission', json={"name": "TagDetecte"})

	# now, we define the destination, which is the location if tag_1 
	move = geometry_msgs.msg.Pose()
	move.position.x = trans.transform.translation.x + tcp_offset_tag['x']
	move.position.y = trans.transform.translation.y + tcp_offset_tag['y']
	move.position.z = trans.transform.translation.z + tcp_offset_tag['z']
	
	loader_service('external_control.urp')
	while state_service().program_name != 'external_control.urp':
		print(state_service())
		time.sleep(0.5)
	player_service()
	time.sleep(1)

	try_cartesian_move(move, False)
	
	# robot arrives at destination, and either picks or places a mug
	urscript_pub.publish('set_digital_out(9, True)')
	time.sleep(2)
	urscript_pub.publish('set_digital_out(9, False)')
	time.sleep(0.5)

	loader_service('external_control.urp')
	while state_service().program_name != 'external_control.urp':
		print(state_service())
		time.sleep(0.5)
	player_service()
	time.sleep(1)

	try_cartesian_move(origin, True)

	return TriggerResponse(True, "MUGPICKED")

def place_mug(req):
	global move_group, tcp_offset_tag

	# the origin position of ur10, which can be editted by yourself if you wish to do so
	origin = move_group.get_current_pose().pose
	print("============ Printing origin")
	print(origin)
	print("")

	tfBuffer = tf2_ros.Buffer()
	listener = tf2_ros.TransformListener(tfBuffer)

	# getting the translation from base_link (base of robot) to tag_1 (any tag which is your destination)
	trans = tfBuffer.lookup_transform('base_link', 'tag_1', rospy.Time(), rospy.Duration(15))
	print("============ Printing tag_1 position")
	print("x: {}".format(trans.transform.translation.x))
	print("y: {}".format(trans.transform.translation.y))
	print("z: {}".format(trans.transform.translation.z))
	print("")

	# now, we define the destination, which is the location if tag_1 
	move = geometry_msgs.msg.Pose()
	move.position.x = trans.transform.translation.x + tcp_offset_tag['x']
	move.position.y = trans.transform.translation.y + tcp_offset_tag['y']
	move.position.z = trans.transform.translation.z + tcp_offset_tag['z']
	
	loader_service('external_control.urp')
	while state_service().program_name != 'external_control.urp':
		print(state_service())
		time.sleep(0.5)
	player_service()
	time.sleep(1)

	try_cartesian_move(move, False)
	
	# robot arrives at destination, and either picks or places a mug	
	urscript_pub.publish('set_digital_out(8, True)')
	time.sleep(2)
	urscript_pub.publish('set_digital_out(8, False)')
	time.sleep(0.5)

	loader_service('external_control.urp')
	while state_service().program_name != 'external_control.urp':
		print(state_service())
		time.sleep(0.5)
	player_service()
	time.sleep(1)

	try_cartesian_move(origin, True)
	
	return TriggerResponse(True, "MUGPLACED")

def make_coffee(req):
	global move_group, tcp_offset_coffee

	# the origin position of ur10, which can be editted by yourself if you wish to do so
	origin = move_group.get_current_pose().pose
	print("============ Printing origin")
	print(origin)
	print("")

	tfBuffer = tf2_ros.Buffer()
	listener = tf2_ros.TransformListener(tfBuffer)

	# getting the translation from base_link (base of robot) to tag_1 (any tag which is your destination)
	trans = tfBuffer.lookup_transform('base_link', 'tag_2', rospy.Time(), rospy.Duration(15))
	print("============ Printing tag_1 position")
	print("x: {}".format(trans.transform.translation.x))
	print("y: {}".format(trans.transform.translation.y))
	print("z: {}".format(trans.transform.translation.z))
	print("")

	# now, we define the destination, which is the location if tag_1 
	move = geometry_msgs.msg.Pose()
	move.position.x = trans.transform.translation.x + tcp_offset_coffee['x']
	move.position.y = trans.transform.translation.y + tcp_offset_coffee['y']
	move.position.z = trans.transform.translation.z + tcp_offset_coffee['z']

	loader_service('external_control.urp')
	while state_service().program_name != 'external_control.urp':
		print(state_service())
		time.sleep(0.5)
	player_service()
	time.sleep(1)

	try_cartesian_move(move, True)

	# robot arrives at destination, and requests a coffee to be made
	requests.get('http://mmdb-coffee-machine/coffee')
	time.sleep(60)

	try_cartesian_move(origin, False)

	return TriggerResponse(True, "IMATEAPOT")

connect_dashboard()

loader_service('external_control.urp')
while state_service().program_name != 'external_control.urp':
	print(state_service())
	time.sleep(0.5)
player_service()
time.sleep(3)

pickMug = rospy.Service('/coffeebot/pickMug', Trigger, pick_mug)
placeMug = rospy.Service('/coffeebot/placeMug', Trigger, place_mug)
makeCoffee = rospy.Service('/coffeebot/makeCoffee', Trigger, make_coffee)

rospy.spin()
