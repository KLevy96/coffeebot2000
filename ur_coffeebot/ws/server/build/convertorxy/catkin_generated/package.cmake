set(_CATKIN_CURRENT_PACKAGE "convertorxy")
set(convertorxy_VERSION "0.0.0")
set(convertorxy_MAINTAINER "olebrun <olebrun@todo.todo>")
set(convertorxy_PACKAGE_FORMAT "2")
set(convertorxy_BUILD_DEPENDS "laser_geometry" "rospy" "sensor_msgs")
set(convertorxy_BUILD_EXPORT_DEPENDS "laser_geometry" "rospy" "sensor_msgs")
set(convertorxy_BUILDTOOL_DEPENDS "catkin")
set(convertorxy_BUILDTOOL_EXPORT_DEPENDS )
set(convertorxy_EXEC_DEPENDS "laser_geometry" "rospy" "sensor_msgs")
set(convertorxy_RUN_DEPENDS "laser_geometry" "rospy" "sensor_msgs")
set(convertorxy_TEST_DEPENDS )
set(convertorxy_DOC_DEPENDS )
set(convertorxy_URL_WEBSITE "")
set(convertorxy_URL_BUGTRACKER "")
set(convertorxy_URL_REPOSITORY "")
set(convertorxy_DEPRECATED "")