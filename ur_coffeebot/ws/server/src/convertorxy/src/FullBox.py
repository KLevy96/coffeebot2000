#!/usr/bin/env python
import rospy
from sensor_msgs.msg import PointCloud2 as pc2
from sensor_msgs.msg import PointCloud as pc
from sensor_msgs.msg import std_msgs as line
from math import *
import math


class FullBox():
    def __init__(self):
	self.Box6 = True
	self.Box7 = True
        self.Box8 = True
        self.Box9 = True
        
	self.Box10 = True
        self.Box11 = True
        self.Box12 = True
        self.Box13 = True
        
	self.Box14 = True
        self.Box15 = True
        self.Box16 = True
        self.Box17 = True

	self.count6 = 0
        self.count7 = 0
        self.count8 = 0
        self.count9 = 0

        self.count10 = 0
        self.count11 = 0
        self.count12 = 0
        self.count13 = 0

        self.count14 = 0
        self.count15 = 0
        self.count16 = 0
        self.count17 = 0

        self.pub1 = rospy.Publisher('StatusLidar', line.msg.String, queue_size=10)
	self.pub2 = rospy.Publisher('StatusBox', line.msg.String, queue_size=10)
        self.laserSub1=rospy.Subscriber("/PointCloudXYZ_1",pc,self.TestBoxLid1)
        self.laserSub2=rospy.Subscriber("/PointCloudXYZ_2",pc,self.TestBoxLid2)
        self.laserSub3=rospy.Subscriber("/PointCloudXYZ_3",pc,self.TestBoxLid3)

    def TestBoxLid1(self,data):
        if data:
	   for angle in data.channels[1].values:
	   	if angle >= 25 and angle <=70:
			self.count7+=1
			if self.count7 > 25:
				self.Box7 = False
		if angle >= 125 and angle <=165:
                        self.count6+=1
                        if self.count6 > 25:
                        	self.Box6 = False
		if angle >= 195 and angle <=240:
                        self.count8+=1
                        if self.count8 > 25:
                        	self.Box8 = False
		if angle >= 290 and angle <=340:
                        self.count9+=1
                        if self.count9 > 25:
                        	self.Box9 = False
	   self.pub1.publish("lidar3UP")
    def TestBoxLid2(self,data):
	if data:
           for angle in data.channels[1].values:
                if angle >= 25 and angle <=70:
                        self.count11+=1
                        if self.count11 > 25:
                        	self.Box11 = False
                if angle >= 125 and angle <=165:
                        self.count10+=1
                        if self.count10 > 25:
                        	self.Box10 = False 
                if angle >= 195 and angle <=240:
                        self.count12+=1
                        if self.count12 > 25:
                        	self.Box12 = False 
                if angle >= 290 and angle <=340:
			self.count13+=1
                        if self.count13 > 25:
                        	self.Box13 = False 
           self.pub1.publish("lidar4UP")
    def TestBoxLid3(self,data):
	if data:
           for angle in data.channels[1].values:
                if angle >= 25 and angle <=70:
                        self.count15+=1
                        if self.count15 > 25:
                        	self.Box15 = False
                if angle >= 125 and angle <=165:
                        self.count14+=1
                        if self.count14 > 25:
                        	self.Box14 = False 
                if angle >= 195 and angle <=240:
                        self.count16+=1
                        if self.count16 > 25:
                        	self.Box16 = False 
                if angle >= 290 and angle <=340:
                        self.count17+=1
                        if self.count17 > 25:
                        	self.Box17 = False 
           self.pub1.publish("lidar5UP")
    def Publish_Box(self,event=None):
	self.pub2.publish("BOX6="+str(self.Box6)+"|BOX7="+str(self.Box7)+"|BOX8="+str(self.Box8)+"|BOX9="+str(self.Box9)+"|BOX10="+str(self.Box10)+"|BOX11="+str(self.Box11)+"|BOX12="+str(self.Box12)+"|BOX13="+str(self.Box13)+"|BOX14="+str(self.Box14)+"|BOX15="+str(self.Box15)+"|BOX16="+str(self.Box16)+"|BOX17="+str(self.Box17))
        self.Box6 = True
        self.Box7 = True
        self.Box8 = True
        self.Box9 = True

        self.Box10 = True
        self.Box11 = True
        self.Box12 = True
        self.Box13 = True

        self.Box14 = True
        self.Box15 = True
        self.Box16 = True
        self.Box17 = True

        self.count6 = 0
        self.count7 = 0
        self.count8 = 0
        self.count9 = 0

        self.count10 = 0
        self.count11 = 0
        self.count12 = 0
        self.count13 = 0

        self.count14 = 0
        self.count15 = 0
        self.count16 = 0
        self.count17 = 0



if __name__ == '__main__':
    rospy.init_node("LidarBox")
    l2pc= FullBox()
    rospy.Timer(rospy.Duration(5.0), l2pc.Publish_Box)
    rospy.spin()
