# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/ws/src/tuw_marker_detection/tuw_aruco/src/aruco_base.cpp" "/ws/build/tuw_marker_detection/tuw_aruco/CMakeFiles/aruco_node.dir/src/aruco_base.cpp.o"
  "/ws/src/tuw_marker_detection/tuw_aruco/src/aruco_marker_pose.cpp" "/ws/build/tuw_marker_detection/tuw_aruco/CMakeFiles/aruco_node.dir/src/aruco_marker_pose.cpp.o"
  "/ws/src/tuw_marker_detection/tuw_aruco/src/aruco_node.cpp" "/ws/build/tuw_marker_detection/tuw_aruco/CMakeFiles/aruco_node.dir/src/aruco_node.cpp.o"
  "/ws/src/tuw_marker_detection/tuw_aruco/src/aruco_parameters.cpp" "/ws/build/tuw_marker_detection/tuw_aruco/CMakeFiles/aruco_node.dir/src/aruco_parameters.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"tuw_aruco\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/ws/devel/include"
  "/ws/src/tuw_marker_detection/tuw_aruco/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "/ws/src/tuw_marker_detection/tuw_aruco/aruco-2.0.10/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/ws/build/tuw_marker_detection/tuw_aruco/aruco-2.0.10/src/CMakeFiles/aruco.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
