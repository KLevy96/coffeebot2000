# CMake generated Testfile for 
# Source directory: /ws/src/tuw_marker_detection/tuw_aruco
# Build directory: /ws/build/tuw_marker_detection/tuw_aruco
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("aruco-2.0.10")
