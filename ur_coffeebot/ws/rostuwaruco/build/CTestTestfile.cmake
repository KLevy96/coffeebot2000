# CMake generated Testfile for 
# Source directory: /ws/src
# Build directory: /ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("tuw_marker_detection/tuw_marker_detection")
subdirs("tuw_marker_detection/tuw_aruco")
subdirs("tuw_marker_detection/tuw_checkerboard")
subdirs("tuw_marker_detection/tuw_ellipses")
subdirs("tuw_marker_detection/tuw_marker_pose_estimation")
