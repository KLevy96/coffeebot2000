#!/usr/bin/env python
import datetime
import re
import traceback

import roslib
# roslib.load_manifest('learning_tf')
import rospy
import math
import tf
import json
from std_msgs.msg import String

REFRESH_RATE = 10
PURGE_DELAY_SECONDS = 1.0 / REFRESH_RATE * 10.0
NUM_DATA_THRESHOLD = 20
LISTENER_CACHE_TIME = PURGE_DELAY_SECONDS / 2.0


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.datetime, datetime.date, datetime.time)):
            return obj.isoformat()
        elif isinstance(obj, datetime.timedelta):
            return (datetime.datetime.min + obj).time().isoformat()

        return json.JSONEncoder.default(self, obj)


class TagProcessing:
    def __init__(self, publisher, debug_publisher, listener, broadcaster):
        self._publisher = publisher
        self._debug_publisher = debug_publisher
        self._listener = listener
        self._broadcaster = broadcaster
        self._datasets = {}
        self._tag_to_size = {}

    def set_tag_to_size(self, tag, size):
        self._tag_to_size[tag] = size

    def get_size_for_tag(self, tag):
        return self._tag_to_size[tag]

    def process_tag(self, tag):
        try:
            tag_center = tag + '_center'

            #self._broadcaster.sendTransform((0.065, -0.0865, 0), (0, 0, 0, 1), rospy.Time.now(), tag_center, tag)
            self._broadcaster.sendTransform((0.084, -0.112, 0), (0, 0, 0, 1), rospy.Time.now(), tag_center, tag)
            (trans, rot) = self._listener.lookupTransform('world', tag_center, rospy.Time(0))

            euler = tf.transformations.euler_from_quaternion(rot)
            roll = euler[0]
            pitch = euler[1]
            yaw = euler[2]
            roll2 = roll * 180 / math.pi
            pitch2 = pitch * 180 / math.pi
            yaw2 = yaw * 180 / math.pi

            #self._debug_publisher.publish('roll={roll2}, pitch={pitch2}, yaw={yaw2}'.format(roll2=roll2, pitch2=pitch2, yaw2=yaw2))

            if trans[0] < -0.10 or trans[0] > 0.7 \
                    or trans[1] < -0.10 or trans[1] > 0.7 \
                    or trans[2] < 0.05 or trans[2] > 0.5 \
                    or yaw2 > 180\
                    or roll2 > 5\
                    or pitch2 > 5:
                return

            if yaw2 > 90:
                yaw2 = yaw2 - 180

            if yaw2 < -90:
                yaw2 = yaw2 + 180

            round_point = (
                round(trans[0] * 2, 2) / 2,
                round(trans[1] * 2, 2) / 2,
                round(trans[2] * 2, 2) / 2,
                round(yaw2)
            )

            if tag not in self._datasets:
                self._datasets[tag] = {
                    'tag': tag,
                    'size': self.get_size_for_tag(tag),
                    'last_update': None,
                    'num_data': 0,
                    'distance_to_origin': round(math.sqrt(round_point[0] ** 2 + round_point[1] ** 2), 3),
                    'clusters': {

                    }
                }

            dataset = self._datasets[tag]
            dataset['num_data'] += 1
            dataset['last_update'] = datetime.datetime.now()

            cluster_id = str(round_point[0]) \
                         + '-' + str(round_point[1]) \
                         + '-' + str(round_point[2]) \
                         + '-' + str(round_point[3])

            if cluster_id not in dataset['clusters']:
                cluster = {
                    'cluster_id': cluster_id,
                    'num_data': 0,
                    'last_update': None,
                    'data': round_point
                }
            else:
                cluster = dataset['clusters'][cluster_id]

            cluster['last_update'] = datetime.datetime.now()
            cluster['num_data'] += 1

            dataset['clusters'][cluster_id] = cluster

            self._datasets[tag] = dataset
        except Exception as e:
            pass
            #self._debug_publisher.publish(json.dumps(traceback.format_exc()))

    def publish_nearest_object(self):
        best_dataset = None
        for tag, dataset in self._datasets.items():
            if not best_dataset or dataset['distance_to_origin'] < best_dataset['distance_to_origin']:
                best_dataset = dataset

        best_cluster = None
        if best_dataset:
            for cluster_id, cluster in best_dataset['clusters'].items():
                if not best_cluster or cluster['num_data'] > best_cluster['num_data']:
                    best_cluster = cluster

        if best_cluster \
                and best_cluster['num_data'] > (best_dataset['num_data'] / 10) \
                and best_dataset['num_data'] > NUM_DATA_THRESHOLD:
            self._publisher.publish(json.dumps({
                'dataset': {
                    'tag': best_dataset['tag'],
                    'size': best_dataset['size'],
                    'num_data': best_dataset['num_data'],
                    'cluster': {
                        'num_data': best_cluster['num_data'],
                        'data': {
                            'x': best_cluster['data'][0],
                            'y': best_cluster['data'][1],
                            'z': best_cluster['data'][2],
                            'a': best_cluster['data'][3]

                        }
                    }
                }
            }))

    def purge_obsolete_datasets_and_clusters(self):
        now = datetime.datetime.now()

        datasets_to_purge = []
        for tag, dataset in self._datasets.items():
            clusters_to_purge = []
            if (now - dataset['last_update']).total_seconds() > PURGE_DELAY_SECONDS:
                datasets_to_purge.append(tag)
            else:
                for cluster_id, cluster in dataset['clusters'].items():
                    if (now - cluster['last_update']).total_seconds() > PURGE_DELAY_SECONDS:
                        clusters_to_purge.append(cluster_id)

                for cluster_id in clusters_to_purge:
                    dataset['num_data'] -= dataset['clusters'][cluster_id]['num_data']
                    del dataset['clusters'][cluster_id]
                    self._debug_publisher.publish('Purge obsolete cluster_id {} for dataset {}'.format(cluster_id, tag))

        for tag in datasets_to_purge:
            del self._datasets[tag]
            self._debug_publisher.publish('Purge obsolete dataset {}'.format(tag))


if __name__ == '__main__':
    pub_status = rospy.Publisher('ednobjtracker/status', String, queue_size=1)
    pub_debug = rospy.Publisher('ednobjtracker/debug', String, queue_size=1)
    pub_object = rospy.Publisher('ednobjtracker/object', String, queue_size=1)

    rospy.init_node('ednobjtracker')

    listener = tf.TransformListener(cache_time=rospy.Duration(1))
    broadcaster = tf.TransformBroadcaster()

    tag_processing = TagProcessing(pub_object, pub_debug, listener, broadcaster)

    for i in range(1, 51):
        tag_processing.set_tag_to_size('tag_{}'.format(i), {
            'width': 0.250,
            'length': 0.315,
            'height': 0.155
        })

    for i in range(51, 101):
        tag_processing.set_tag_to_size('tag_{}'.format(i), {
            'width': 0.210,
            'length': 0.275,
            'height': 0.140
        })

    for i in range(101, 151):
        tag_processing.set_tag_to_size('tag_{}'.format(i), {
            'width': 0.250,
            'length': 0.250,
            'height': 0.115
        })

    rate = rospy.Rate(REFRESH_RATE)
    while not rospy.is_shutdown():
        try:
            pub_status.publish(json.dumps({
                'status': 'up',
                'refresh_rate': REFRESH_RATE,
                'purge_delay_seconds': PURGE_DELAY_SECONDS,
                'num_data_threshold': NUM_DATA_THRESHOLD,
                'listener_cache_time': LISTENER_CACHE_TIME
            }))

            for t in listener.getFrameStrings():
                if re.search('^tag_[1-9][0-9]*$', t):
                    tag_processing.process_tag(t)

            tag_processing.purge_obsolete_datasets_and_clusters()
            tag_processing.publish_nearest_object()

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            pass
            #pub_debug.publish(json.dumps(traceback.format_exc()))
        finally:
            rate.sleep()
