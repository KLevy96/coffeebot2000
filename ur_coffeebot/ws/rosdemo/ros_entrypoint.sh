#!/bin/bash

set -e
source /opt/ros/melodic/setup.bash
catkin_make
source /ws/devel/setup.bash
exec "$@"
