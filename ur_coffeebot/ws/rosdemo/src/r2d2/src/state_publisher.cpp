#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>


int main(int argc, char** argv) {
    ros::init(argc, argv, "state_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

    tf::TransformBroadcaster broadcaster;
    ros::Rate loop_rate(100);  //speed

    const double pas = 0.01;
    
    // robot origin state
    double x_pos=0;
    double y_pos=0;

    // fixed marker position
    const double x_marker=3;
    const double y_marker=2;


    // message declarations
    geometry_msgs::TransformStamped odom_trans;
    sensor_msgs::JointState joint_state;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "axis";
   

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
   

        // update transform

        odom_trans.header.stamp = ros::Time::now();
        if (x_pos < x_marker-0.005)
        {
	x_pos = x_pos + pas; 
        odom_trans.transform.translation.x = (x_pos)*1;
	}
        else if (x_pos > x_marker+0.005)
	{
	x_pos = x_pos - pas; 
        odom_trans.transform.translation.x = (x_pos)*1;
	}
	else
	{
	odom_trans.transform.translation.x = (x_pos)*1;
	}
        if (y_pos < y_marker-0.005)
	{
	y_pos = y_pos + pas;
        odom_trans.transform.translation.y = (y_pos)*1;
	}
        else if (y_pos > y_marker+0.005)
	{
	y_pos = y_pos - pas;
        odom_trans.transform.translation.y = (y_pos)*1;
	}
	else
	{
	odom_trans.transform.translation.y = (y_pos)*1;
	}
        odom_trans.transform.translation.z = 0.1;
        odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(0);

        //send the joint state and transform
        joint_pub.publish(joint_state);
        broadcaster.sendTransform(odom_trans);

     

       

        // This will adjust as needed per iteration
        loop_rate.sleep();
    }


    return 0;
}
