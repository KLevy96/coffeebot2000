# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/ws/src/fmauch_universal_robot/ur_msgs/msg/Analog.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/Digital.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/IOStates.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/RobotStateRTMsg.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/MasterboardDataMsg.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/RobotModeDataMsg.msg;/ws/src/fmauch_universal_robot/ur_msgs/msg/ToolDataMsg.msg"
services_str = "/ws/src/fmauch_universal_robot/ur_msgs/srv/SetPayload.srv;/ws/src/fmauch_universal_robot/ur_msgs/srv/SetSpeedSliderFraction.srv;/ws/src/fmauch_universal_robot/ur_msgs/srv/SetIO.srv"
pkg_name = "ur_msgs"
dependencies_str = "std_msgs;geometry_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "ur_msgs;/ws/src/fmauch_universal_robot/ur_msgs/msg;std_msgs;/opt/ros/melodic/share/std_msgs/cmake/../msg;geometry_msgs;/opt/ros/melodic/share/geometry_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python2"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/melodic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
