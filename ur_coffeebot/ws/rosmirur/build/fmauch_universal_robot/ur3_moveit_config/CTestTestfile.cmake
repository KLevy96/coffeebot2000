# CMake generated Testfile for 
# Source directory: /ws/src/fmauch_universal_robot/ur3_moveit_config
# Build directory: /ws/build/fmauch_universal_robot/ur3_moveit_config
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_ur3_moveit_config_roslaunch-check_tests_moveit_planning_execution.xml "/ws/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/ws/build/test_results/ur3_moveit_config/roslaunch-check_tests_moveit_planning_execution.xml.xml" "--return-code" "/usr/bin/cmake -E make_directory /ws/build/test_results/ur3_moveit_config" "/opt/ros/melodic/share/roslaunch/cmake/../scripts/roslaunch-check -o \"/ws/build/test_results/ur3_moveit_config/roslaunch-check_tests_moveit_planning_execution.xml.xml\" \"/ws/src/fmauch_universal_robot/ur3_moveit_config/tests/moveit_planning_execution.xml\" ")
