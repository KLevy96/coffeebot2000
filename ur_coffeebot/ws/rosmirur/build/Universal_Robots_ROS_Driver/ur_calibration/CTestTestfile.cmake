# CMake generated Testfile for 
# Source directory: /ws/src/Universal_Robots_ROS_Driver/ur_calibration
# Build directory: /ws/build/Universal_Robots_ROS_Driver/ur_calibration
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_ur_calibration_gtest_calibration_test "/ws/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/ws/build/test_results/ur_calibration/gtest-calibration_test.xml" "--return-code" "/ws/devel/lib/ur_calibration/calibration_test --gtest_output=xml:/ws/build/test_results/ur_calibration/gtest-calibration_test.xml")
