# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/msg/ProgramState.msg;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/msg/RobotMode.msg;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/msg/SafetyMode.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeAction.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeActionGoal.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeActionResult.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeActionFeedback.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeGoal.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeResult.msg;/ws/devel/share/ur_dashboard_msgs/msg/SetModeFeedback.msg"
services_str = "/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/AddToLog.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/GetLoadedProgram.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/GetProgramState.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/GetRobotMode.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/GetSafetyMode.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/IsProgramRunning.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/IsProgramSaved.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/Load.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/Popup.srv;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/srv/RawRequest.srv"
pkg_name = "ur_dashboard_msgs"
dependencies_str = "std_msgs;actionlib_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "ur_dashboard_msgs;/ws/src/Universal_Robots_ROS_Driver/ur_dashboard_msgs/msg;ur_dashboard_msgs;/ws/devel/share/ur_dashboard_msgs/msg;std_msgs;/opt/ros/melodic/share/std_msgs/cmake/../msg;actionlib_msgs;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python2"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/melodic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
