#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/ws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/ws/build'
export PYTHONPATH="/ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/ws/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/ws/src:$ROS_PACKAGE_PATH"