#!/bin/bash

set -e
source /opt/ros/melodic/setup.bash

rosdep update
rosdep install --from-paths src --ignore-src -y
catkin_make
source /ws/devel/setup.bash
exec "$@"
