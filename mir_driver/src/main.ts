import { Mission } from './mission';
import { app } from './server';
export { mir_ip, httpHeaders };

const shajs = require('sha.js');
const base64 = require('base-64');

// Global Variables
const mir_ip: string = process.argv[3];
const mir_user: string = process.argv[4];
const mir_pwd_hash: string = shajs('sha256').update(process.argv[5]).digest('hex');
const mir_token: string = base64.encode(mir_user + ':' + mir_pwd_hash);

const httpHeaders: any = {
    'Content-Type': 'application/json',
    'Accept-Language': 'en_US',
    'Host': mir_ip + ':8080',
    'Authorization': 'Basic ' + mir_token
}

Mission.populateMissions();

app.listen(
    3000,
    () => {
        console.log('[Express] Web server running on port 3000.');
    }
);

// Reliability Checks
setInterval(() => {
    Mission.populateMissions();
}, 60*1000);