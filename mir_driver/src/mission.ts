import {mir_ip, httpHeaders} from './main';
export {Mission};

const axios = require('axios');


class Mission {
    name: string;
    guid: string;
    
    static mission_list: Mission[] = [];
    static mission_namemap: Mission[] = [];
    static mission_guidmap: Mission[] = [];

    constructor(name: string, guid: string) {
        this.name = name;
        this.guid = guid;
    }
    
    async queue(): Promise<void> {
        await axios.post(
            'http://' + mir_ip + '/api/v2.0.0/mission_queue',
            {
                mission_id: this.guid,
                message: '',
                parameters: [],
                priority: 0
            },
            {
                headers: httpHeaders
            }
        ).then(
            (res: any) => {
                console.log('\t' + res.status + ' ' + res.statusText);
            }
        ).catch(
            (error: any) => {
                console.log(error);
            }
        )
    };

    static populateMissions(): void {
        axios.get(
            'http://' + mir_ip + '/api/v2.0.0/missions',
            {'headers': httpHeaders}
        ).then(
            (res: any) => {
                console.log('<= populateMissions()');

                res.data.forEach(
                    (mission: any) => {
                        let m = new Mission(mission.name, mission.guid);

                        console.log('\t' + mission.name + ' ' + mission.guid);

                        Mission.mission_list.push(m);
                        Mission.mission_namemap[mission.name] = m;
                        Mission.mission_guidmap[mission.guid] = m;
                    }
                );
            }
        ).catch(
            (error: any) => {
                console.log(error);
            }
        );
    }
}