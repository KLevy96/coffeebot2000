export { ros, pickMug, placeMug, makeCoffee, voidReq };

const ROSLIB = require('roslib');

// Global variables
const ros_ip: string = process.argv[2];
let ros_up: boolean = false;

// ROS 
let ros = new ROSLIB.Ros({
    url: 'ws://' + ros_ip + ':9090'
});

const pickMugService: any = new ROSLIB.Service({
    ros: ros,
    name: '/coffeebot/pickMug',
    serviceType: 'std_srvs/Trigger'
});

const placeMugService: any = new ROSLIB.Service({
    ros: ros,
    name: '/coffeebot/placeMug',
    serviceType: 'std_srvs/Trigger'
});

const makeCoffeeService: any = new ROSLIB.Service({
    ros: ros,
    name: '/coffeebot/makeCoffee',
    serviceType: 'std_srvs/Trigger'
});

async function pickMug(): Promise<void> {
    return new Promise((resolve: any, reject: any) => {
        pickMugService.callService(voidReq, (res: any) => {
            console.log('\t[Coffee] Mug has been picked');
            resolve();
        });
    });
}

async function placeMug(): Promise<void> {
    return new Promise((resolve: any, reject: any) => {
        placeMugService.callService(voidReq, (res: any) => {
            console.log('\t[Coffee] Mug has been placed');
            resolve();
        });
    });
}

async function makeCoffee(): Promise<void> {
    return new Promise((resolve: any, reject: any) => {
        makeCoffeeService.callService(voidReq, (res: any) => {
            console.log('\t[Coffee] Coffee has been made');
            resolve();
        });
    });
}

ros.on('connection', () => {
    ros_up = true;
    console.log('[ROS] Connected to websocket server.');
});

ros.on('error', error => {
    ros_up = false;
    ros.close();
    console.log('[ROS] Error connecting to websocket server: ', error);
});

ros.on('close', () => {
    ros_up = false;
    console.log('[ROS] Connection to websocket server closed.');
});

const voidReq: any = new ROSLIB.ServiceRequest({});

setInterval(() => {
    if (!ros_up) ros.connect();
}, 5000);