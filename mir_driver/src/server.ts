import { Mission } from './mission';
// import {mir_ip, httpHeaders} from './main';
import { pickMug, placeMug, makeCoffee, voidReq } from './ros';
import { Status } from './status';
export { app };

const express = require('express');
const bodyParser = require('body-parser') ;
const cors = require('cors');


const app = express();
let mirStatus = new Status();
let inUse: boolean = false;

app.use(cors());
app.use(bodyParser.json());

app.get(
    '/getMissions',
    (req: any, res: any) => {
        console.log('=> /getMissions');

        res.json(Mission.mission_list);
    }
);

app.post(
    '/serveCoffee',
    async (req: any, res: any) => {
        console.log('=> /serveCoffee');

        if (inUse) {
            res.sendStatus(400, 'Robot is in use.');
        } else {
            inUse = true;

            if (req.body.name && Mission.mission_namemap[req.body.name]) {
                try {
                    await Mission.mission_namemap[req.body.name].queue();

                    await mirStatus.waitForStatus(Status.READY);
                    console.log('\t<= pickMug()');
                    await pickMug();

                    await Mission.mission_namemap['PreCafe'].queue();
                    await Mission.mission_namemap['Cafe'].queue();

                    console.log('\t<= makeCoffee()');
                    await mirStatus.waitForStatus(Status.READY);
                    await makeCoffee();

                    await Mission.mission_namemap['PostCafe'].queue();
                    await Mission.mission_namemap[req.body.name].queue();

                    await mirStatus.waitForStatus(Status.READY);
                    console.log('\t<= placeMug()');
                    await placeMug();

                    await Mission.mission_namemap['Charge'].queue();

                    inUse = false;

                    res.sendStatus(200);
                } catch (e) {
                    console.log(e);
                    inUse = false;
                    res.sendStatus(400);
                    await Mission.mission_namemap['Charge'].queue();
                }
            } else {
                inUse = false;
                res.sendStatus(400);
            }
        }
    }
);

app.post(
    '/playMission',
    (req: any, res: any) => {
        console.log('=> /playMission');

        if (req.body.name && Mission.mission_namemap[req.body.name]) {
            Mission.mission_namemap[req.body.name].queue();
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
        }
    }
);