import { mir_ip, httpHeaders } from './main';
export { Status };

const axios = require('axios');

class Status {
    static READY: number = 3;
    static PAUSE: number = 4;
    static EXECUTING: number = 5;

    static uptime: number;
    static battery_percentage: number;
    static state_id: number;
    static state_text: string;

    constructor() {
        setInterval(
            () => {
                this.update();
            },
            1000
        );
    }

    private update(): void {
        axios.get(
            'http://' + mir_ip + '/api/v2.0.0/status',
            {
                headers: httpHeaders
            }
        ).then(
            (res: any) => {
                Status.uptime = res.data.uptime;
                Status.battery_percentage = res.data.battery_percentage;
                Status.state_id = res.data.state_id;
                Status.state_text = res.data.state_text;
            }
        ).catch(
            (error: any) => {
                console.log(error);
            }
        )
    }

    async waitForStatus(state_id): Promise<void> {
        return new Promise((resolve: any, reject: any) => {
            let statusCounter = 0;
            function check() {
                console.log('\tStatus: ' + Status.state_id + ', Counter: ' + statusCounter);
                if (Status.state_id === state_id) {
                    statusCounter++;
                    if (statusCounter > 5) {
                        console.log('\tLet\'s go !');
                        resolve();
                    } else {
                        setTimeout(check, 1000);
                    }
                } else {
                    statusCounter = 0;
                    setTimeout(check, 1000);
                }
            }
            check();
        });
    }
}